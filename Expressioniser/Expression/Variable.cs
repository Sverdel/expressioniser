﻿namespace Expressioniser.Expression
{
    internal readonly struct Variable
    {
        public Variable(char letter, int power)
        {
            Letter = letter;
            Power = power;
        }

        public char Letter { get;}

        public int Power { get; }

        public override string ToString() => $"{Letter}{(Power != 1 ? "^" + Power : string.Empty)}";
    }
}
