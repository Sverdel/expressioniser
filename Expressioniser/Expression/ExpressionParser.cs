﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Expressioniser.Expression
{
    public static class ExpressionParser
    {
        private const char _plus = '+';
        private const char _minus = '-';
        private const char _equals = '=';
        private const char _asterisk = '*';
        private const char _caret = '^';
        private const char _whitespace = ' ';
        private const char _open = '(';
        private const char _close = ')';

        public static string Parse(string input)
        {
            var expression = new Expression(input);
            var isNegative = false;
            var result = new List<Node>();
            while (expression.CanIterate())
            {
                if (expression.SafeCheck(c => c == _equals))
                {
                    expression.MoveNext();
                    if (isNegative)
                    {
                        return "Expression contains multiple equals signs";
                    }
                    isNegative = true;
                    continue;
                }

                expression.Skip();

                try
                {
                    foreach (var node in ReadNode(expression, isNegative))
                    {
                        var i = result.FindIndex(x => x.CanSum(node));
                        if (i > -1)
                        {
                            result[i] += node;
                        }
                        else
                        {
                            result.Add(node);
                        }
                    }
                }
                catch (ParseException ex)
                {
                    return ex.Message;
                }
                catch (Exception ex)
                {
                    return $"Unexpected exception on expression processing: {ex.Message}";
                }
            }

            return !isNegative
                ? "Expression does not contains equals sign"
                : string.Join(_whitespace, result.Where(x => x.Coefficient != 0)
                                                 .OrderByDescending(x => x.Variables.FirstOrDefault().Power)
                                                 .ThenBy(x => x.Variables.FirstOrDefault().Letter))
                         .Trim(_plus, _whitespace) + " = 0";
        }

        private static IEnumerable<Node> ReadNode(Expression expression, bool isNegative = false)
        {
            var node = new Node(isNegative);
            if (expression.SafeCheck(c => c == _close))
            {
                throw new ParseException("Unopened parenthesis");
            }

            expression.Skip(_plus);
            ProcessSign(expression, node);

            expression.Skip();

            ProcessCoefficient(expression, node);

            expression.Skip(_asterisk);

            var checkSymbols = new[] { _plus, _minus, _equals, _open, _close };
            ProcessVariables(expression, node, checkSymbols);

            expression.Skip();
            return expression.SafeCheck(c => c == _open)
                ? ProcessParentheses(ref expression, node)
                : new List<Node> { node };
        }

        private static void ProcessSign(Expression expression, Node node)
        {
            if (!expression.SafeCheck(c => c == _minus))
            {
                return;
            }
            
            expression.MoveNext();
            node.Coefficient *= -1;
        }

        private static void ProcessCoefficient(Expression expression, Node node)
        {
            if (!expression.SafeCheck(char.IsDigit))
            {
                return;
            }
            
            if (!expression.TryReadFloat(out var coefficient))
            {
                throw new ParseException("Invalid float in expression");
            }

            node.Coefficient *= coefficient;
        }

        private static void ProcessVariables(Expression expression, Node node, char[] checkSymbols)
        {
            while (expression.SafeCheck(c => !checkSymbols.Contains(c)))
            {
                var letter = _whitespace;
                var power = 1;
                if (expression.SafeCheck(char.IsLetter))
                {
                    letter = expression.Read();
                }

                if (expression.SafeCheck(c => c == _caret))
                {
                    expression.MoveNext();
                    expression.Skip();
                    if (!expression.TryReadInt(out power))
                    {
                        throw new ParseException("Invalid power number");
                    }
                }

                if (char.IsWhiteSpace(letter)
                    || !node.Variables.Add(new Variable(letter, power)))
                {
                    throw new ParseException("Invalid addend in expression");
                }

                expression.Skip(_asterisk);
            }
        }

        private static IEnumerable<Node> ProcessParentheses(ref Expression expression, Node node)
        {
            expression.MoveNext();
            expression.Skip();

            var nodes = new List<Node>();
            while (expression.SafeCheck(c => c != _close))
            {
                if (!expression.CanIterate() || expression.SafeCheck(c => c == _equals))
                {
                    throw new ParseException("Unclosed parenthesis");
                }

                nodes.AddRange(ReadNode(expression).Select(child => node * child));

                expression.Skip();
            }

            expression.MoveNext();
            expression.Skip();

            return nodes;
        }
    }
}
