﻿using System;
using System.Globalization;
using System.Linq;

namespace Expressioniser.Expression
{
    internal class Expression
    {
        private const char _dot = '.';
        private readonly ReadOnlyMemory<char> _expression;
        
        private int _index;

        public Expression(string input)
        {
            _expression = input.AsMemory();
            _index = 0;
        }

        public void MoveNext() => _index++;

        public char Read() => _expression.Span[_index++];

        public bool CanIterate() => _index < _expression.Length;

        public void Skip(params char[] symbols) =>
           Skip(c => char.IsWhiteSpace(c) || symbols.Contains(c));

        public bool SafeCheck(Func<char, bool> condition) => _index < _expression.Length && condition(_expression.Span[_index]);

        public bool TryReadInt(out int number)
        {
            var start = _index;
            Skip(char.IsDigit);

            return int.TryParse(_expression.Span.Slice(start, _index - start), out number);
        }

        public bool TryReadFloat(out float number)
        {
            var start = _index;
            Skip(c => char.IsDigit(c) || c == _dot);

            return float.TryParse(_expression.Span.Slice(start, _index - start), NumberStyles.AllowThousands | NumberStyles.Float, CultureInfo.InvariantCulture, out number);
        }
        
        private void Skip(Func<char, bool> condition)
        {
            while (SafeCheck(condition))
            {
                _index++;
            }
        }
    }
}
