﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Expressioniser.Expression
{
    internal class Node
    {
        private static readonly Comparer<Variable>  _comparer = Comparer<Variable>.Create((a, b) =>
            {
                var result = -1 * a.Power.CompareTo(b.Power); // sort port descending
                return result != 0 ? result : a.Letter.CompareTo(b.Letter);
            });

        public Node(bool isNegative = false)
        {
            Coefficient = isNegative ? -1 : 1;
            Variables = new SortedSet<Variable>(_comparer);
        }

        public float Coefficient { get; set; }

        public SortedSet<Variable> Variables { get; private set; }

        public bool CanSum(Node node) => Variables.SequenceEqual(node.Variables);

        public static Node operator +(Node a, Node b)
            => new Node
            {
                Coefficient = a.Coefficient + b.Coefficient,
                Variables = a.Variables
            };

        public static Node operator *(Node a, Node b)
        {
            var dict = a.Variables.ToDictionary(x => x.Letter, y => y.Power);
            foreach (var variable in b.Variables)
            {
                if (!dict.ContainsKey(variable.Letter))
                {
                    dict.Add(variable.Letter, variable.Power);
                }
                else
                {
                    dict[variable.Letter] += variable.Power;
                }
            }

            return new Node
            {
                Coefficient = a.Coefficient * b.Coefficient,
                Variables = new SortedSet<Variable>(dict.Select(x => new Variable(x.Key, x.Value)), _comparer)
            };
        }

        public override string ToString() => Coefficient == 0
            ? string.Empty
            : $"{(Coefficient < 0 ? "-" : "+")} " +
              $"{GecCoefficientString()}" +
              $"{string.Join(string.Empty, Variables.Select(x => x.ToString()))}";

        private string GecCoefficientString()
        {
            var coefficient = Math.Abs(Coefficient);
            return Variables.Count == 0 || coefficient != 1
                ? coefficient.ToString(CultureInfo.InvariantCulture)
                : string.Empty;
        }
    }
}
