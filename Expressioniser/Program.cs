﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CommandLine;
using Expressioniser.Expression;
using Expressioniser.Options;

namespace Expressioniser
{
    internal class Program
    {
        public static int Main(string[] args)
        {
            return Parser.Default.ParseArguments<FileModeOptions, InteractiveModeOptions>(args)
                .MapResult(
                  (FileModeOptions opts) =>  RunFileProcessing(opts).GetAwaiter().GetResult(),
                  (InteractiveModeOptions opts) => RunInteractiveMode(opts),
                  errs => 1);
        }

        private static int RunInteractiveMode(InteractiveModeOptions opts)
        {
            Console.CancelKeyPress += (s, e) =>
            {
                e.Cancel = true;
                Environment.Exit(0);
            };

            Console.WriteLine("Press Ctrl + C to stop the program..");

            while (true)
            {
                try
                {
                    Console.WriteLine("Input expression to parse:");
                    var expression = Console.ReadLine();
                    if (string.IsNullOrEmpty(expression))
                    {
                        continue;
                    }

                    var result = ExpressionParser.Parse(expression);
                    Console.WriteLine($"Canonical form: {result}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unexpected exception on expression processing: {0}", ex.Message);
                    Console.WriteLine("Please check your expression");
                }
            }
        }

        private static async Task<int> RunFileProcessing(FileModeOptions opts)
        {
            try
            {
                if (!File.Exists(opts.InputFilePath))
                {
                    Console.WriteLine("Input file does not exists");
                    return 1;
                }

                if (!Directory.Exists(opts.OutPath))
                {
                    Console.WriteLine("Output folder does not exists");
                    return 1;
                }

                var result = new List<string>();
                foreach (var line in await File.ReadAllLinesAsync(opts.InputFilePath))
                {
                    result.Add(ExpressionParser.Parse(line));
                }

                var outFileName = Path.Combine(opts.OutPath, $"{Path.GetFileNameWithoutExtension(opts.InputFilePath)}.out");
                await using (var file = File.CreateText(outFileName))
                {
                    foreach (var line in result)
                    {
                        await file.WriteLineAsync(line);
                    }
                }

                Console.WriteLine("Process complete. You can find results in {0}", outFileName);

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unexpected exception on file processing {0}", ex.Message);
                return 1;
            }
        }
    }
}
