﻿using CommandLine;

namespace Expressioniser.Options
{
    [Verb("file", HelpText = "Process expressions from input file")]
    internal class FileModeOptions
    {
        [Option('o', "out", Required = true, HelpText = "Output folder path")]
        public string OutPath { get; set; }

        [Option('i', "in", Required = true, HelpText = "Input file path")]
        public string InputFilePath { get; set; }
    }
}
