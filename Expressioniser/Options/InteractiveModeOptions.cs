﻿using CommandLine;

namespace Expressioniser.Options
{
    [Verb("interactive", HelpText = "Interactive expressions parse mode")]
    internal class InteractiveModeOptions
    {
    }
}
