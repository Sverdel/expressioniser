using Expressioniser.Expression;
using Xunit;

namespace Expressioniser.Tests
{
    public class Tests
    {
        [Theory]
        [InlineData("y = x", "- x + y = 0")]
        [InlineData("x + - y = x", "- y = 0")]
        [InlineData("y = 1 - x ", "x + y - 1 = 0")]
        [InlineData("y + 3 = x", "- x + y + 3 = 0")]
        [InlineData("2x + y = 2x", "y = 0")]
        [InlineData("2x + y = 2y + x", "x - y = 0")]
        [InlineData("2xy + y = 2yx + x", "- x + y = 0")]
        [InlineData("2*x*y + y = 2yx + x", "- x + y = 0")]
        [InlineData("x^2 + 3.5xy + y = y^2 - xy + y", "x^2 - y^2 + 4.5xy = 0")]
        [InlineData("x^2 + 3.5x^2y + y = y^2 - xy + y", "x^2 + 3.5x^2y - y^2 + xy = 0")]
        [InlineData("x^2 + 3.5yx^2 + y = y^2 - xy + y", "x^2 + 3.5x^2y - y^2 + xy = 0")]
        [InlineData("8b^7+9y-9b+9c+8x-1a^4=4b-3c^5+5b^5c^4x+6a-c", "8b^7 - 5b^5c^4x + 3c^5 - a^4 - 6a - 13b + 10c + 8x + 9y = 0")]
        [InlineData("2(x + y) = 3x(4y^2 + 8x^3)", "- 24x^4 - 12y^2x + 2x + 2y = 0")]
        [InlineData("2(x + 3(y + x)) = 3x(4y^2 + 8x^3)", "- 24x^4 - 12y^2x + 8x + 6y = 0")]
        [InlineData("2*(x + 3*(y + x)) = 3x*(4y^2 + 8x^3)", "- 24x^4 - 12y^2x + 8x + 6y = 0")]
        public void ExpressionParserTest(string input, string expectedResult)
        {
            var result = ExpressionParser.Parse(input);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData("x", "Expression does not contains equals sign")]
        [InlineData("x^b = y", "Invalid power number")]
        [InlineData("x^2^3 = y", "Invalid addend in expression")]
        [InlineData("x/a = y", "Invalid addend in expression")]
        [InlineData("y = y$x", "Invalid addend in expression")]
        [InlineData("y + 3 = xx", "Invalid addend in expression")]
        [InlineData("y + 3 = x = y", "Expression contains multiple equals signs")]
        [InlineData("y + 3.4.5 = x", "Invalid float in expression")]
        [InlineData("y + 3.4 ( = x", "Unclosed parenthesis")]
        [InlineData("y + 3.4 ) = x", "Unopened parenthesis")]
        public void ExpressionParserInvalidFormatTest(string input, string message)
        {
            var result = ExpressionParser.Parse(input);
            Assert.Equal(message, result);
        }
    }
}
